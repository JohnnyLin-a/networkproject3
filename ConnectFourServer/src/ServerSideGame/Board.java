package ServerSideGame;

/**
 * Board class will hold game check methods and board data.
 * 
 * @author Johnny
 */
public class Board {
	private final int rows = 12;
	private final int cols = 13;
	private int[][] gameBoard;
        private int p1_token = 21;
        private int p2_token = 21;
        
        /**
         * Default constructor creates the board and fills them with 0;
         */
	public Board () {
            gameBoard = new int[rows][cols];
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    this.gameBoard[i][j] = 0;
                }
            }
	}
        
        /**
         * Displays the board on the console.
         */
        public void displayBoard() {
            System.out.println("Board:");
            System.out.println("0 1 2 3 4 5 6 7 8 9 0 1 2 ");
            System.out.println("==========================");
            for (int i = 0; i < rows; i++) {
                for (int j = 0; j < cols; j++) {
                    System.out.print(this.gameBoard[i][j]+" ");
                }
                System.out.println();
            }
            System.out.println("==========================");
            System.out.println("0 1 2 3 4 5 6 7 8 9 0 1 2 ");
        }
        
        /**
         * Play the token in the board and checks if the game status is changed.
         * 
         * @param col column to play at.
         * @param player 1= player 1 2= AI
         * @return gameStatus if the a player won or tied.
         */
        public int play(int col, int player) {
            if (!(player == 1 || player == 2)) {
                System.out.println("Player " + player + " is not a real player.");
                return 99;
            }
            if (!isPlayable(col)) {
                System.out.println("Player "+player+" made an impossible move at column " +col+".");
                return 99; //99 = error code
            }
            if (player == 1 && p1_token == 0) {
                System.out.println("Player 1: out of token! Game is a tie.");
                return 53;
            } else if (player == 2 && p2_token == 0) {
                System.out.println("Player 2: out of token! Gmae is a tie.");
                return 53;
            }
            
            //finds row to play to
            int i = findRowToPlay(col);
            
            
            //removes a token, plays the token there
            this.gameBoard[i][col] = player;
            if (player == 1) {
                this.p1_token--;
            } else {
                this.p2_token--;
            }
            
            
            
            int gameStatus = 0;
            //check for if no one has tokens anymore
            if (this.p1_token == 0 && this.p2_token == 0) {
                gameStatus = 53;
            }
            int newGameStatus = checkGame(i,col,player);
            if (newGameStatus != 0) {
                return newGameStatus;
                //if one wins with last token
            }
            return gameStatus;
        }
        
        /**
         * Check score if played in which row and column for which player.
         * 
         * @param row row
         * @param col column
         * @param player 1= player 1 2= AI
         * @return score
         */
        public int checkScore(int row, int col, int player) {
            int original_score = 0;
            int score = 0;
            
            //check horizontal (left then right)
            original_score = checkHorizontal(row, col, player);
            score = checkVertical(row,col,player);
            if (score > original_score) original_score = score;
            score = checkDiag1(row,col,player);
            if (score > original_score) original_score = score;
            score = checkDiag2(row,col,player);
            if (score > original_score) original_score = score;
            
            return original_score;
        }
        
        private int checkGame(int row, int col, int player) {
            int score = checkScore(row,col,player);
            if (score < 3) {
                return 0;
            } else {
                if (player == 1) {
                    return 51;
                } else {
                    return 52;
                }
            }
        }
        
        /**
         * Checks if the column is playable.
         * 
         * @param col column
         * @return true= Can play false= Cannot play
         */
        public boolean isPlayable(int col) {
            if (gameBoard[0][col] == 0)
                return true;
            else
                return false;
        }
        
        /**
         * Gets a copy of the board.
         * @return gameBoard
         */
        public int[][] getGameBoard() {
            return gameBoard;
        }

        private int checkHorizontal(int row, int col, int player) {
            int original_row = row;

            int score = 0;
            // check left before right
            while (row-1 > -1) {
                if (this.gameBoard[row-1][col] == player) {
                    score++;
                    row-=1;
                } else {
                    break;
                }
            }
            row = original_row;
            while (row+1 < 12) {
                if (this.gameBoard[row+1][col] == player) {
                    score++;
                    row+=1;
                } else {
                    break;
                }
            }

            return score;
        }

    private int checkVertical(int row, int col, int player) {
        int original_col = col;
        int score = 0;
        
        //check up before down
        while (col-1 > -1) {
            if (this.gameBoard[row][col-1] == player) {
                score++;
                col-=1;
            } else {
                    break;
                }
        }
        col = original_col;
        while (col+1 < 13) {
            if (this.gameBoard[row][col+1] == player) {
                score++;
                col+=1;
            } else {
                    break;
                }
        }
        return score;
    }

    private int checkDiag1(int row, int col, int player) {
        //check increasing
        int original_col = col;
        int original_row = row;
        int score = 0;
        
        //check approaching to the left
        while (row+1 < 12 && col-1 > -1) {
            if (this.gameBoard[row+1][col-1] == player) {
                score++;
                col-=1;
                row+=1;
            } else {
                    break;
                }
        }
        col = original_col;
        row = original_row;
        //check approaching to the right
        while (row-1 > -1 && col+1 < 13) {
            if (this.gameBoard[row-1][col+1] == player) {
                score++;
                col+=1;
                row-=1;
            } else {
                    break;
                }
        }
        return score;
    }

    private int checkDiag2(int row, int col, int player) {
        //check decreasing
        int original_col = col;
        int original_row = row;
        int score = 0;
        
        //check approaching to the right
        while (row+1 < 12 && col+1 < 13) {
            if (this.gameBoard[row+1][col+1] == player) {
                score++;
                col+=1;
                row+=1;
            } else {
                    break;
                }
        }
        col = original_col;
        row = original_row;
        //check approaching to the left
        while (row-1 > -1 && col-1 < -1) {
            if (this.gameBoard[row-1][col-1] == player) {
                score++;
                col-=1;
                row-=1;
            } else {
                    break;
                }
        }
        return score;
    }
    
    /**
     * Sets the board to the new board.
     * 
     * @param gameBoard gameBoard
     */
    public void setGameBoard(int[][] gameBoard) {
        this.gameBoard = gameBoard;
    }
    
    /**
     * Get value in a exact row and column.
     * 
     * @param row row
     * @param col column
     * @return 0=empty 1=Player 1 2=AI
     */
    public int get(int row, int col) {
        return this.gameBoard[row][col];
    }
    
    /**
     * Finds the row to where the token will end up after the token drops in the
     * board.
     * 
     * @param col column
     * @return row
     */
    public int findRowToPlay(int col) {
        int i = 11;
        while (i >= 0 && gameBoard[i][col] != 0) {
            i--;
        }
        return i;
    }
}

        
        

