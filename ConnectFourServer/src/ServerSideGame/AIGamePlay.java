package ServerSideGame;

/**
 * Logic of AI player
 *
 * @author Felicia Gorgatchov
 *
 */
public class AIGamePlay {

    /**
     * MAIN LOGIC
     *
     * check entire array if ai can win directly, if can win, return coords. If
     * not, find the opponent highest move, whichever it is (1, 2, or 3) play
     * there. If the game just started and no move brings any points, return
     * coords of a random position at the bottom of the grid.
     *
     * @param board -- current game board
     * @return -- coords where token should be placed -- [0] = row -- [1] = col
     */
    public int[] getBestMove(int[][] board) {
        int player = 1;
        int ai = 2;

        int[] coords = new int[2];
        int highestPoints = 0;
        int currentPoints = 0;

        // check if it's a direct win, yes --> return
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (canPlay(board, i, j)) {
                    currentPoints = getMovePoints(board, i, j, ai);
                    if (currentPoints == 3) {
                        coords[0] = i;
                        coords[1] = j;
                        return coords;
                    }
                }
            }
        }

        // block strongest opponent move --> for highest points record coords
        for (int i = 0; i < board.length; i++) {
            for (int j = 0; j < board[i].length; j++) {
                if (canPlay(board, i, j)) {
                    currentPoints = getMovePoints(board, i, j, player);
                    if (currentPoints >= highestPoints) {
                        highestPoints = currentPoints;
                        coords[0] = i;
                        coords[1] = j;
                    }
                }
            }
        }

        // return the coords of the move with the highest points found
        if (highestPoints > 0 && highestPoints <= 3) {
            return coords;
        } // else if game just started and no movepoints --> select a random position
        else if (currentPoints == 0) {
            int randCol = (int) ((Math.random() * ((board[1].length - 0) + 1)) + 0);
            coords[0] = board.length - 1;
            coords[1] = randCol;
        }
        return coords;
    }

    /**
     * HELPER 
     * 
     * will verify the number of points a line to the right, bottom right
     * diagonal, bottom line, or bottom left diagonal brings as well as the sum of two sides
     * in case there is a win by playing in the middle of a row (e.g. 1 0 1 1) and return the
     * highest points
     *
     * @param board -- current game board
     * @param row -- current row coord
     * @param col -- current col coord
     * @param token -- check if following tokens match the ones we're looking
     * for
     * @return
     */
    private int getMovePoints(int[][] board, int row, int col, int token) {
        int highestPoints = 0;
        int currentPoints = 0;
        int firstSide = 0;
        int secondSide = 0;

        //check left
        for (int i = 1; i < 4; i++) {
            if (col - i < 0) {
                break;
            } else {
                if (isTokenMatch(board, token, row, col - i)) {
                    firstSide++;
                } else {
                    break;
                }
            }
        }

        // check right
        for (int i = 1; i < 4; i++) {
            if (col + i > 12) {
                break;
            } else {
                if (isTokenMatch(board, token, row, col + i)) {
                    secondSide++;
                } else {
                    break;
                }
            }
        }
        
        if (firstSide + secondSide > highestPoints) {
            highestPoints = firstSide + secondSide;
        }
        firstSide = 0;
        secondSide = 0;

        //check top left corner
        for (int i = 1; i < 4; i++) {
            if (row - i < 0 || col - i < 0) {
                break;
            }
            if (isTokenMatch(board, token, row - i, col - i)) {
                firstSide++;
            } else {
                break;
            }
        }       

        // check bottom right corner
        for (int i = 1; i < 4; i++) {
            if (row + i > 11 || col + i > 12) {
                break;
            }
            if (isTokenMatch(board, token, row + i, col + i)) {
                secondSide++;
            } else {
                break;
            }
        }
        
        if (firstSide + secondSide > highestPoints) {
            highestPoints = firstSide + secondSide;
        }
        firstSide = 0;
        secondSide = 0;

        // check below
        for (int i = 1; i < 4; i++) {
            if (row + i > 11) {
                break;
            } else {
                if (isTokenMatch(board, token, row + i, col)) {
                    currentPoints++;
                } else {
                    break;
                }
            }
        }

        if (currentPoints > highestPoints) {
            highestPoints = currentPoints;
        }
        currentPoints = 0;

        //check top right corner
        for (int i = 1; i < 4; i++) {
            if (row - i < 0 || col + i > 12) {
                break;
            } else {
                if (isTokenMatch(board, token, row - i, col + i)) {
                    firstSide++;
                } else {
                    break;
                }
            }
        }

        // check bottom left corner
        for (int i = 1; i < 4; i++) {
            if (row + i > 11 || col - i < 0) {
                break;
            } else {
                if (isTokenMatch(board, token, row + i, col - i)) {
                    secondSide++;
                } else {
                    break;
                }
            }
        }

        if (firstSide + secondSide > highestPoints) {
            highestPoints = firstSide + secondSide;
        }

        return highestPoints;

    }

    /**
     * HELPER
     *
     * checks if the token we're checking corresponds to the player token we're
     * looking for
     *
     * @param board -- current game board
     * @param token -- player we are looking to match
     * @param row -- row coord of current position
     * @param col -- col coord of current position
     * @return -- true if the token is what we're looking for -- false if
     * doesn't match
     */
    private boolean isTokenMatch(int[][] board, int token, int row, int col) {
        if (board[row][col] == token) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * HELPER
     *
     * checks if the tile below the current one is played or if on the last row.
     * If it is not 0, there's a token. AI can play in both cases. It is to
     * ensure the AI "drops" a token on the grid and doesn't just place it
     * anywhere.
     *
     * @return true if something below or last row, false if array tile beneath
     * is 0
     */
    private boolean canPlay(int[][] board, int row, int col) {
        //check if there's a token bellow
        if ((row == board.length - 1 || (board[row + 1][col] != 0)) && board[row][col] == 0) {
            return true;
        }
        return false;
    }

}
