/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package C4Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * Server thread that gets instantiated every time a new client connects
 * 
 * @author Felicia Gorgatchov
 */
public class C4MultiThreadServer implements Runnable{
    private final int serverPort = 50000;
    private ServerSocket serverSock;
    private Socket clientSock;
    /**
     * Constructor that takes a client socket as a parameter
     * @param client
     */
    public C4MultiThreadServer(Socket client) {       
        this.clientSock = client;
    }
    /**
     * Starts a server side game of connect four
     */
    public void run() {
        try {
            C4GameInterface game = new C4GameImpl(clientSock);
            game.startGame();
        } catch (IOException ioe) {
            System.out.println("The game disconnected. " + ioe.getMessage());
        }
        
    }
    
}
