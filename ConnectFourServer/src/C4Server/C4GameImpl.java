package C4Server;

import C4Server.Packet.C4Connection;
import ServerSideGame.AIGamePlay;
import ServerSideGame.Board;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;

public class C4GameImpl implements C4GameInterface {
    private final C4Connection connection;
    private AIGamePlay ai;
    private Board board;
    private int gameStatus;

    /**
     * Constructor accepts a client in the form of a socket. It set's up
     * everything that is needed to run the game logic.
     * 
     * @param clientSock client connection
     * @throws IOException 
     */
    public C4GameImpl(Socket clientSock) throws IOException{
        this.connection = new C4Connection(clientSock);
    }
    
    @Override
    public void startGame() throws IOException {
        while (true){
            connection.read();
            System.out.println("Player: " + connection.getIpAndPort());
            System.out.println("Player sent this packet: " + this.connection.getPacket()[0]+","+this.connection.getPacket()[1]);
            if(this.connection.getPacket()[1] == 50) { //new game byte received
                if (this.ai == null || this.board == null || this.ai == null) {// game not started
                    if (this.connection.getPacket()[1] == 50) {
                        this.ai = new AIGamePlay();
                        this.board = new Board();
                        this.connection.write(0,50); //start game bytes
                    }
                }
            } else { //game has started
                if (connection.getPacket()[1] == 0) {
                    //Game continues situation
                    gameStatus = this.board.play(connection.getPacket()[0], 1);
                    this.board.displayBoard();
                            
                    if (gameStatus == 0) {
                        //no one won yet
                        //ai's turn
                        int bestMove[] = ai.getBestMove(this.board.getGameBoard());
                        gameStatus = this.board.play(bestMove[1], 2);
                        this.board.displayBoard();
                        connection.write(bestMove[1], gameStatus);
                        if (gameStatus != 0) {
                            System.out.println("AI changed gameStatus to: " + gameStatus);
                            System.out.println("server will close client connection");
                            connection.close();
                            break;
                        }
                    }
                    
                } else if (connection.getPacket()[1] == 51 || connection.getPacket()[1] == 52 || connection.getPacket()[1] == 53) {
                    //reached game end situation
                    gameStatus = this.connection.getPacket()[1];
                    if (gameStatus == 51) {
                        System.out.println("Gratz! P1 won.");
                        connection.write(-1, gameStatus);
                        break;
                    } else if (gameStatus == 52) {
                        System.out.println("Gratz! P2 won.");
                        connection.write(-1, gameStatus);
                        break;
                    } else if ( gameStatus == 53) {
                        System.out.println("No one won. It's a tie!");
                        connection.write(-1, gameStatus);
                        break;
                    }
                } else if (connection.getPacket()[1] == -1) {
                    //reached quit situation
                    System.out.println("P1 quit, closing connection.");
                    connection.close();
                    break;
                } else if (connection.getPacket()[1] == 99) {
                    //reached error situation
                    System.out.println("ERROR: recieved error status code 99. will close connections.");
                    connection.close();
                    break;
                }
            }
        }
    }
    
    
    
}
