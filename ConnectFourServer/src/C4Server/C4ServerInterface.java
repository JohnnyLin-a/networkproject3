package C4Server;

/**
 * C4Server class will handle incoming connection to start playing Connect 4.
 * 
 * @author Johnny
 */
public interface C4ServerInterface {
    /**
     * Loops incoming connections and starts a game of Connect Four.
     */
    public void start();
}
