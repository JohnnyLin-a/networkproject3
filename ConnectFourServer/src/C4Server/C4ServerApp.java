/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package C4Server;

/**
 * Main application class. Will create a new server and start it.
 * 
 * @author Johnny
 */
public class C4ServerApp {
    public static void main(String[] args) {
        C4ServerInterface server= new C4ServerImpl();
        
        server.start();
    }
}
