package C4Server.Test;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * Connect Four Connection Test application. Will display what packet is sent
 * back to client. This application mainly tests if packet sending and receiving
 * works.
 * 
 * WARNING! This class has nothing to do with the Connect Four game itself!
 * 
 * 
 * @author Johnny
 */
public class C4ConnectionTest {
    /**
     * Test class to manually send packets to the server without going through the game logic
     * @param args
     * @throws IOException 
     */
    public static void main(String[] args) throws IOException {
        String ip = "localhost";
        int port = 50000;
        String mode = "1"; //mode "0" send only, mode "1" with packet received
        final int BYTE_BUFFER = 2;
        String send;
        
        System.out.println("Attempt to connect to " + ip);
        Socket socket = new Socket(ip, port);
        
        System.out.println("Getting IOStreams...");
        InputStream in = socket.getInputStream();
        OutputStream out = socket.getOutputStream();
        DataInputStream d_in = new DataInputStream(in);
        
        System.out.println("Setting up what to write");
        Scanner k = new Scanner(System.in);
        byte[] b = new byte[BYTE_BUFFER];
        
        if(mode.equals("1")) {
            System.out.println("Mode 1 selected.");
        }
        
        //Sends and receives packets to and from the server.
        while (true) {
            try {
                System.out.print("Send (col,server code): ");
                send = k.nextLine();
                System.out.println("Send: " + send);
                b[0] = (byte)Integer.parseInt(send.substring(0,send.indexOf(',')));
                b[1] = (byte)Integer.parseInt(send.substring(send.indexOf(',')+1,send.length()));
                System.out.println("Attempting to send this: "+b[0]+","+b[1]);
                out.write(b);
                
                if (mode.equalsIgnoreCase("1")) {
                    d_in.readFully(b);
                    System.out.println("Server responded with: "+b[0]+","+b[1]);
                    if (b[1] == -1) {
                        System.out.println("Game exit on server side, no other packets will be received");
                        break;
                    }
                }
            } catch (NumberFormatException e) {
                System.out.println("You messed up your input ...");
                System.out.println(e.getMessage());
            }
        }
    } 
}

