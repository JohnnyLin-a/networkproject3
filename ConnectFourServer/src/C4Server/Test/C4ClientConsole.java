package C4Server.Test;

import C4Server.Packet.C4Connection;
import ServerSideGame.Board;
import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;

/**
 * The Connect four client used for when playing the server. Text based.
 * WARNING! This client does not check for invalid inputs.
 * 
 * @author Johnny
 */
public class C4ClientConsole {
    /**
     * Test class to manually send packets to the server without going through the game logic
     * @param args runtime arguments
     * @throws IOException connection exception
     */
    public static void main(String[] args) throws IOException {
        //initializing required variables
        Board board = new Board();
        C4Connection connection;
        Scanner k = new Scanner(System.in);
        
        //Getting Server info
        System.out.print("Please enter the ip address: ");
        String ip = k.nextLine();
        int port = 50000;
        System.out.println("Attempt to connect to " + ip);
        Socket socket = new Socket(ip, port);
        connection = new C4Connection (socket);
        
        //initializing game variables.
        int received_gameStatus = 0;
        int col = 0;
        int gameStatus = 0;
        int p1_score = 0;
        int p2_score = 0;
        
        //start of game (sends 0,50 then receives 0,50)
        connection.write(0, 50);
        connection.read();
        
        //Displays the board to let player play.
        board.displayBoard();
        
        //main game loop to play until game is over.
        while (true) {
            //asking for player's turn play
            System.out.print("Play (col) -1 to exit: ");
            col = Integer.parseInt(k.nextLine());
            if (col == -1) System.exit(0);
            System.out.println("Playing: " + col);
            
            /*
                Plays the token played by the player. Adds score.
                Records if someone has won/lost/tied the game
                Sends the player's move to server.
                Finally displays the board after the Player played.
            */
            p1_score += board.checkScore(board.findRowToPlay(col), col, 1);
            gameStatus = board.play(col, 1);
            connection.write(col, gameStatus);
            board.displayBoard();
            
            //check errors (gameStatus is the error code 99 or an exit code -1)
            if (gameStatus == 99 || gameStatus == -1) {
                System.out.println("error happened, game is closing");
                System.exit(gameStatus);
            }
            
            //check status after p1 plays (51 = player win, 53 = game tied)
            if (gameStatus == 51) {
                System.out.println("You win!");
                System.out.println("Player score: " + p1_score);
                System.out.println("AI score: " + p2_score);
                System.exit(0);
            } else if (gameStatus == 53) {
                System.out.println("It's a tie!");
                System.out.println("Player score: " + p1_score);
                System.out.println("AI score: " + p2_score);
                System.exit(0);
            }
            
            //read connection bytes and gets AI move and gameStatus
            connection.read();
            col = connection.getPacket()[0];
            received_gameStatus = connection.getPacket()[1];
            
            
            //check error packet(99) or exit packet(-1)
            if (gameStatus == 99 || received_gameStatus == -1) {
                System.out.println("error happened, game is closing");
                System.exit(gameStatus);
            }
            
            /*
                Plays the token played by the AI. Adds score.
                Records if AI has won/lost/tied the game.
                Finally displays the board after the AI played.
            */
            p2_score += board.checkScore(board.findRowToPlay(col), col, 2);
            gameStatus = board.play(col, 2);
            board.displayBoard();
            System.out.println("AI played: col "+ col);
            
            
            //check error packet(99) or exit packet(-1)
            if (gameStatus == 99 || gameStatus == -1) {
                System.out.println("error happened, game is closing");
                System.exit(gameStatus);
            }
            
            //check status after AI played (52 = AI win, 53 = game tied)
            if (gameStatus == 52) {
                System.out.println("AI win!");
                System.out.println("Player score: " + p1_score);
                System.out.println("AI score: " + p2_score);
                System.exit(0);
            } else if (gameStatus == 53) {
                System.out.println("It's a tie!");
                System.out.println("Player score: " + p1_score);
                System.out.println("AI score: " + p2_score);
                System.exit(0);
            }
        }
    } 
}

