/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package C4Server.Packet;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * C4Connection manages packet connection between server and client.
 * 
 * @author Johnny
 */
public class C4Connection {
    /*
        special packet codes:
        b->99 error
        b->50 game start
        b->51 Player wins
        b->52 AI wins
        b->53 Tie
        b->-1 Quit
    */
    private Socket socket;
    private InputStream in;
    private OutputStream out;
    private DataInputStream d_in;
    
    private byte[] packet = {0,0};
    
    /**
     * Constructor. Sets all input and output streams for the class to use and
     * send data.
     * 
     * @param socket client/server socket
     * @throws IOException connection exception
     */
    public C4Connection(Socket socket) throws IOException{
        this.socket = socket;
        SocketAddress clientAddress = socket.getRemoteSocketAddress();
        this.in = socket.getInputStream();
        this.out = socket.getOutputStream();
        this.d_in = new DataInputStream(in);
        
        System.out.println("User has connected. IP: " + clientAddress);
    }
    
    /**
     * getter for the packet received/sent/to send
     * @return packet
     */
    public byte[] getPacket() {
        return packet;
    }
    
    /**
     * setter for the packet to received/sent/to send
     * @param packet 
     */
    public void setPacket(byte[] packet) {
        this.packet = packet;
    }
    
    /**
     * Sends the packet to the server/client.
     * 
     * @param b0 First byte
     * @param b1 Second byte
     * @throws IOException connection exception
     */
    public void write(int b0, int b1) throws IOException{
        this.packet[0] = (byte) b0;
        this.packet[1] = (byte) b1;
        System.out.println("Sending back: "+ b0 + ","+b1);
        this.out.write(this.packet);
    }
    
    /**
     * Reads incoming packet from server/client
     * 
     * @throws IOException connection exception
     */
    public void read() throws IOException{
        d_in.readFully(packet);
    }
    
    /**
     * Closes all input/output streams.
     * 
     * @throws IOException connection exception
     */
    public void close() throws IOException {
        socket.close();
        in.close();
        out.close();
        d_in.close();
    }
    
    public String getIpAndPort() {
        return socket.getRemoteSocketAddress().toString();
    }
}
