package C4Server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;

/**
 * C4ServerImpl class implements C4ServerInterface.
 * This class accepts connections and start the game for the client.
 * @author Johnny
 */
public class C4ServerImpl implements C4ServerInterface{
    private final int serverPort = 50000;
    private ServerSocket serverSock;
    
    /**
     * Default constructor for Connect four server.
     */
    public C4ServerImpl() {
    }
    /**
     * Opens a loop that starts listening for new connections and assigns new threads
     * in response to client side calls.
     */
    @Override
    public void start() {
        try {
            serverSock = new ServerSocket(serverPort);
            System.out.println("Server hosted at: "+InetAddress.getLocalHost().getHostAddress()+":"+serverPort);
            // Infinite loop that keeps accepting new connections until the server closes
            while (true) {
                try{
                    System.out.println("Accepting new connection...");
                    Socket clientSock = serverSock.accept();
                    new Thread(new C4MultiThreadServer(clientSock)).start();
                }
                catch(Exception e){
                    System.out.println("Problem accepting client connection: " + e.getStackTrace());
                }
            }
        } catch (IOException ioe) {
            System.out.println("Some stuff happened to the server. "+ioe.getMessage());
        }
    }
}
