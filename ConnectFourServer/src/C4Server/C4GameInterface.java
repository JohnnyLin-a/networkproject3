package C4Server;

import java.io.IOException;
import java.net.Socket;

/**
 * This class contains all of the game logic, including AI and packet sending.
 * 
 * @author Johnny
 */
public interface C4GameInterface {
    /**
     * Main game loop method. Will accept packets, and play the game with the
     * client. This includes AI, packet handling and game related logic.
     * 
     * @throws IOException Connection exception
     */
    public void startGame() throws IOException;
}
