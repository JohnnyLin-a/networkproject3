/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfourgui;

import java.io.IOException;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * MTStage stands for Multi Threaded Stage.
 * MTStage contains a normal stage for threaded execution.
 * 
 * @author Johnny Lin
 */
public class MTStage implements Runnable {
    private Stage stage;
    private Parent root;
    private Scene scene;
    
    /**
     * Run will setup the stage, scene and title and open it.
     */
    @Override
    public void run() {
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                System.out.println("Running thread...");
                try {
                    stage = new Stage();
                    System.out.println("Loading fxml...");
                    setRoot(FXMLLoader.load(getClass().getResource("GameWindow.fxml")));
                    setScene(new Scene(getRoot()));
                    //Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();

                    stage.setTitle("Game on!");
                    stage.setScene(getScene());
                    stage.centerOnScreen();
                    stage.show();
                } catch (IOException ioe) {
                    System.out.println("Error!: " + ioe.getMessage());
                }
            }
        });
        
    }
    
    //Default getters and setters below for class variables
    public Stage getStage() {
        return stage;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public Parent getRoot() {
        return root;
    }

    public void setRoot(Parent root) {
        this.root = root;
    }

    public Scene getScene() {
        return scene;
    }

    public void setScene(Scene scene) {
        this.scene = scene;
    }
    
    
    
}
