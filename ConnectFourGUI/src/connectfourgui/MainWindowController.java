/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package connectfourgui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * Controller for the main window where the ip is given as input
 * 
 * @author Stanislav Yurchenko
 * @version 05/12/2018
 */
public class MainWindowController implements Initializable {

    @FXML
    private TextField ip;

    @FXML
    private TextField port;
	/**
	 * Play game button event handler
         * Saves the ip and port into history and launches a new Thread to run the game.
	 * 
	 * @param event
	 */
	@FXML
	private void handleButtonAction(ActionEvent event) {
                //Store ip and port to last run history.
		Properties p = new Properties();
		try (OutputStream os = new FileOutputStream("project.properties")) {
			p.setProperty("ip", ip.getText());
			p.setProperty("port", port.getText());
			p.store(os, null);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
                
                //Run new Thread to play game

                try {
                    Thread t = new Thread(new MTStage());
                    t.start();
                    System.out.println("Thread started: " + t.getName());
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
	}
        
        
        /**
         * Set the text fields to last connection info
         * @param url url
         * @param rb resourceBundle
         */
	@Override
	public void initialize(URL url, ResourceBundle rb) {
		Properties p = new Properties();
		try (InputStream is = new FileInputStream("project.properties")){
			p.load(is);
			ip.setText(p.getProperty("ip", ""));
			port.setText(p.getProperty("port", ""));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
