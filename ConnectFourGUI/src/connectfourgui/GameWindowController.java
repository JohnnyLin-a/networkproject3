package connectfourgui;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.ConnectException;
import java.net.Socket;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.ResourceBundle;

import C4Server.Packet.C4Connection;
import ServerSideGame.Board;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * Controller for the window in which the grid appears
 * 
 * @author Stanislav Yurchenko
 * @version 05/12/2018
 *
 */
public class GameWindowController implements Initializable {
	private int p1_score = 0;
	private int p2_score = 0;
	private C4Connection connection;
	private Board board;
	@FXML
	private GridPane grid;
	@FXML
	private Label connectString;

	/**
	 * Back button event handler
	 * Closes the active game.
	 * @param event
	 */
	@FXML
	private void handleButtonAction(ActionEvent event) {
                try {
                    this.connection.write(0, -1);
                    this.connection.close();
                } catch (IOException ioe) {
                    System.out.println("Error: " + ioe.getMessage());
                }
                Stage stage = (Stage) ((Button) event.getSource()).getScene().getWindow();
                stage.close();
	}

	/**
	 * Hovering over a circle will highlight the whole column to give feedback of
	 * where to play
	 * 
	 * @param event
	 */
	@FXML
	private void hover(MouseEvent event) {
		Circle c = (Circle) event.getSource();
		int loc = GridPane.getColumnIndex(c);
		ArrayList<Rectangle> wholeColumn = findAllByColumn(loc);
		for (Rectangle r : wholeColumn) {
			r.setFill(Color.YELLOW);
		}
	}

	/**
	 * Removing the mouse from above a circle will stop highlighting an area for it
	 * cannot be clicked any longer
	 * 
	 * @param event
	 */
	@FXML
	private void unhover(MouseEvent event) {
		Circle c = (Circle) event.getSource();
		int loc = GridPane.getColumnIndex(c);
		ArrayList<Rectangle> wholeColumn = findAllByColumn(loc);
		for (Rectangle r : wholeColumn) {
			r.setFill(Color.DODGERBLUE);
		}
	}

	/**
	 * Clicking on a chip
	 * 
	 * @param event
	 */
	@FXML
	private void click(MouseEvent event) {
		int stat = 0;
		Circle c = (Circle) event.getSource();
		c = getLowest(GridPane.getColumnIndex(c));
		if (c != null && c.getFill().toString().equals("0xd4eaffff")) {
			try {
				// set score gotten from p1
				p1_score += board.checkScore(board.findRowToPlay(GridPane.getColumnIndex(c)),
						GridPane.getColumnIndex(c), 1);
				stat = board.play(GridPane.getColumnIndex(c), 1);
				connection.write(GridPane.getColumnIndex(c), stat);
				c.setFill(Color.ROYALBLUE);

				// Switch statement for player moves response
				switch (stat) {
				case 99:
					giveMessage("Error");
					break;
				case -1:
					giveMessage("Connection closed");
					break;
				case 51:
					giveMessage("You won " + getScoreString());
					break;
				case 53:
					giveMessage("No one wins " + getScoreString());
					break;
				}
				connection.read();
				// set p2 score
				p2_score += board.checkScore(board.findRowToPlay(connection.getPacket()[0]), connection.getPacket()[0],
						2);
				System.out.println("Got packet: " + connection.getPacket()[0] + ", " + connection.getPacket()[1]);
				stat = playRed(connection.getPacket()[0]);
				// Switch statement for computer's move response
				switch (stat) {
				case 99:
					giveMessage("Error");
					break;
				case -1:
					giveMessage("Connection closed");
					break;
				case 52:
					giveMessage("Computer wins " + getScoreString());
					break;
				case 53:
					giveMessage("No one wins " + getScoreString());
					break;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Helper method that finds the lowest inputtable position
	 * 
	 * @param column
	 * @return lowest circle in a column
	 */
	private Circle getLowest(int column) {
		Circle c = null;
		for (int i = 0; i < 12; i++) {
			c = findByLocation(column, i);
			if (!c.getFill().toString().equals("0xd4eaffff")) {
				c = findByLocation(column, i - 1);
				break;
			}
		}
		return c;
	}

	/**
	 * Helper method that finds a circle by its location
	 * 
	 * @param x
	 * @param y
	 * @return circle at location
	 */
	private Circle findByLocation(int x, int y) {
		Circle c = null;
		for (Node n : grid.getChildren()) {
			if (n instanceof Circle && GridPane.getRowIndex(n) != null && GridPane.getColumnIndex(n) != null) {
				if (GridPane.getColumnIndex(n) == x && GridPane.getRowIndex(n) == y) {
					c = (Circle) n;
					break;
				}
			}
		}
		return c;
	}

	private ArrayList<Rectangle> findAllByColumn(int col) {
		ArrayList<Rectangle> r = new ArrayList<Rectangle>();
		for (Node n : grid.getChildren()) {
			if (n instanceof Rectangle && GridPane.getRowIndex(n) != null && GridPane.getColumnIndex(n) != null) {
				if (GridPane.getColumnIndex(n) == col) {
					r.add((Rectangle) n);
				}
			}
		}
		return r;
	}

	/**
	 * Plays the computer's chips
	 * 
	 * @param col
	 *            to be played on
	 * @return game status from the computer side
	 */
	private int playRed(int col) {
		Circle c = getLowest(col);
		int stat = board.play(col, 2);
		c.setFill(Color.RED);
		return stat;
	}

	/**
	 * This method displays a confirmation message passed to it Also closes the
	 * connection and closes the stage
	 * 
	 * @param message
	 */
	private void giveMessage(String message) {
		Alert dialog = new Alert(AlertType.CONFIRMATION, message, ButtonType.OK);
		dialog.showAndWait();
                try {
                        connection.close();
                        
                } catch (IOException e) {
                        e.printStackTrace();
                }
                
                Stage stage = (Stage) connectString.getScene().getWindow();
                stage.close();
	}

	/**
	 * Initialises a new board to play on, reads the ip and port from properties and
	 * sends out the will to play bytes
	 */
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		board = new Board();
		Properties p = new Properties();
		try (InputStream is = new FileInputStream("project.properties")) {
			p.load(is);
			connectString.setText("IP: " + p.getProperty("ip", "") + ":" + p.getProperty("port", ""));
			connection = new C4Connection(new Socket(p.getProperty("ip"), Integer.parseInt(p.getProperty("port"))));
			connection.write(0, 50);
			connection.read();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			if (e instanceof UnknownHostException) {
				giveMessage("Incorrect IP");
			} else if (e instanceof ConnectException) {
				giveMessage("Incorrect port");
			}
			e.printStackTrace();
		}
	}

	private String getScoreString() {
		return "Scores: Player: " + p1_score + "  AI: " + p2_score;
	}
}
